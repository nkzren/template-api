const stan = require("node-nats-streaming");
const { USER_CREATED, USER_DELETED } = require('./util/topic');

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");
    const db = mongoClient.db().collection('pingr');

    const userCreatedSub = conn.subscribe(USER_CREATED);
    userCreatedSub.on('message', async (message) => {
      const event = JSON.parse(message.getData());
      const user = event.entityAggregate;
      user._id = event.entityId;

      await db.insertOne(user);
      console.log(`Inserted user succesfully ${user}`);
    });

    const userDeletedSub = conn.subscribe(USER_DELETED);
    userDeletedSub.on('message', async (message) => {
      const event = JSON.parse(message.getData());
      const userId = event.entityId;

      await db.deleteOne({ _id: userId });
      console.log(`Deleted user succesfully ${userId}`);
    })

  });

  return conn;
};
