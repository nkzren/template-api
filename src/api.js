const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");
const { OK, CREATED, BAD_REQUEST, UNAUTHORIZED, FORBIDDEN, UNPROCESSABLE_ENTITY } = require('./util/statusCode');
const { USER_CREATED, USER_DELETED } = require('./util/topic');

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  const db = mongoClient.db().collection('pingr');

  const validateRequest = (request, response, next) => {
    const body = request.body;

    const requiredFields = ["name", "email", "password", "passwordConfirmation"];

    const missingFields = requiredFields.filter(e => !body[e]);
    if (missingFields.length) {
      const missingString = missingFields.reduce((result, e) => `${result}, ${e}`);
      response.status(BAD_REQUEST).json({ error: `Request body had missing field(s): ${missingString}` });
    } else {
      next();
    }
  }

  const validateNewUserFields = async (request, response, next) => {
    const body = request.body;

    if (!body.name) {
      response.status(BAD_REQUEST).json(errorMalformedFieldMsg("name"));
    } else if (await validateEmail(body.email)) {
      response.status(BAD_REQUEST).json(errorMalformedFieldMsg("email"));
    } else if (validatePassword(body.password)) {
      response.status(BAD_REQUEST).json(errorMalformedFieldMsg("password"));
    } else {
      next();
    }
  }

  async function validateEmail(email) {
    const emailPresent = await db.findOne({ email });
    const emailRegex = /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/g;

    return !email.length || !email.match(emailRegex) || emailPresent;
  }

  function validatePassword(pass) {
    return !pass.match(/^[a-zA-Z0-9]{8,32}$/g);
  }

  const validatePasswordConfirmation = (request, response, next) => {
    const body = request.body;

    if (body.password !== body.passwordConfirmation) {
      response.status(UNPROCESSABLE_ENTITY).json({ error: 'Password confirmation did not match' });
    } else {
      next();
    }
  }

  const hasToken = (request, response, next) => {
    if (!request.get("Authorization")) {
      response.status(UNAUTHORIZED).json({ error: "Access Token not found" });
    } else {
      next();
    }
  }

  const validateToken = (request, response, next) => {
    const authorization = request.get("Authorization");

    const token = authorization.split(" ")[1];
    const decoded = jwt.decode(token);
    if (decoded.id !== request.params.uuid) {
      response.status(FORBIDDEN).json({ error: "Access Token did not match User ID" });
    } else {
      next()
    }

  }


  const newUserValidations = [validateRequest, validateNewUserFields, validatePasswordConfirmation];
  api.post("/users", newUserValidations, async (request, response) => {
    const newUser = request.body;

    const newUserId = uuid();
    const newUserEvent = {
      "eventType": USER_CREATED,
      "entityId": newUserId,
      "entityAggregate": {
        "name": newUser.name,
        "email": newUser.email,
        "password": newUser.password
      }
    }
    stanConn.publish(USER_CREATED, JSON.stringify(newUserEvent));

    response.status(CREATED).json({
      user: {
        id: newUserId,
        name: newUser.name,
        email: newUser.email
      }
    });
  });

  const deleteValidations = [ hasToken, validateToken ]
  api.delete("/users/:uuid", deleteValidations, async (request, response) => {
    const id = request.params.uuid;

    const deleteUserEvent = {
      "eventtype": USER_DELETED,
      "entityId": id,
      "entityAggregate": {}
    }

    stanConn.publish(USER_DELETED, JSON.stringify(deleteUserEvent));

    const user = await db.findOne({ _id: id })
    if (!user) {
      response.status(OK).json({
        "id": id
      });
    }

  })


  return api;
};

function errorMalformedFieldMsg(fieldName) {
  return {
    error: `Request body had malformed field ${fieldName}`
  };
}